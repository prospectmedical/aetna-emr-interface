﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class Processed
#Region "Declarations"
	Private miProcessedID As Int32
#End Region

#Region "Class Properties"
	Public Property ErrorMessage As String
#End Region

#Region "Routines"
	Public Sub New()
    Initialize()
  End Sub

  Public Sub Initialize()
    miProcessedID = 0
		ErrorMessage = vbNullString
	End Sub
#End Region

#Region "Insert"
	Public Function Insert(ProcessingTypeID As Int32, ProcessingDate As String, ProcessingTime As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		Insert = False
		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Processed.Insert: Error opening the database.")
			oCmd.CommandText = "iProcessed"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessingTypeID", OleDbType.BigInt).Value = ProcessingTypeID
			oCmd.Parameters.Add("@ProcessingDate", OleDbType.VarChar, 10).Value = ProcessingDate
			oCmd.Parameters.Add("@ProcessingTime", OleDbType.VarChar, 5).Value = ProcessingTime
			oCmd.Parameters.Add("@NewID", OleDbType.BigInt).Direction = ParameterDirection.Output
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			miProcessedID = SetInt32(oCmd.Parameters("@NewID").Value)
			Insert = True
		Catch oException As Exception
			ErrorMessage = oException.Message
			Insert = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "Completed"
	Public Function Completed(ProcessingTypeID As Int32, ProcessingDate As String, ProcessingTime As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing

		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Processed-Complete: Error opening database.")
			oCmd.CommandText = "sProcessedCompleted"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessingTypeID", OleDbType.BigInt).Value = ProcessingTypeID
			oCmd.Parameters.Add("@ProcessingDate", OleDbType.VarChar, 10).Value = ProcessingDate
			oCmd.Parameters.Add("@ProcessingTime", OleDbType.VarChar, 5).Value = ProcessingTime
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			Completed = oRdr.HasRows
		Catch oException As Exception
			ErrorMessage = oException.Message
			Initialize()
			Completed = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "EMRProcessed"
	Public Function EMRProcessed(ByVal Type As String, ByVal SubType As String) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand
    Dim oRdr As OleDbDataReader = Nothing
    Dim sRunDate As String = Format$(DateAdd(DateInterval.Day, -1, Now), "yyyy-MM-dd")

		ErrorMessage = vbNullString
		Try
      If Not OpenDB(oDBC, 3) Then Throw New Exception("Processed.Complete: Error opening database.")
      oCmd.CommandText = "sProcessedByTypeSubTypeRunDate"
      oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ServerName", OleDbType.VarChar, 25).Value = gsEMRServer
			oCmd.Parameters.Add("@DatabaseName", OleDbType.VarChar, 25).Value = gsEMRDatabase
      oCmd.Parameters.Add("@Type", OleDbType.VarChar, 100).Value = Type
      oCmd.Parameters.Add("@SubType", OleDbType.VarChar, 100).Value = SubType
      oCmd.Parameters.Add("@RunDate", OleDbType.VarChar, 10).Value = sRunDate
      oCmd.Connection = oDBC
      oRdr = oCmd.ExecuteReader
      oRdr.Read()
			EMRProcessed = oRdr.HasRows
		Catch oException As Exception
      EMRProcessed = False
			ErrorMessage = oException.Message
		Finally
      CloseRDR(oRdr)
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region
End Class


