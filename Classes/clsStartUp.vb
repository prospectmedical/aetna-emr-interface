#Region "Init"
Option Strict On
Option Explicit On
Imports System.Reflection
Imports System.Security.Principal
#End Region

Public Class StartUp
#Region "Class Properties"
	Public Property ErrorMessage As String
#End Region

#Region "StartUp Routine"
	Public ReadOnly Property Run() As Boolean
		Get
			Dim oInitializeApplication As New InitializeApplication
			Dim oWindowsPrincipal As WindowsPrincipal
			Dim oWindowsIdentity As WindowsIdentity

			Try
				gsAppPath = ApplicationStartupPath()
				gsGraphicsPath = AddBackslash(gsAppPath) & "Graphics\"
				gsHTMLPath = AddBackslash(gsAppPath) & "HTML\"
				gsEMRServer = "SQLDB04"
				gsEMRDatabase = "EMR01"
				With Assembly.GetExecutingAssembly.GetName.Version
					gsVersion = .Major.ToString & "." & .Minor.ToString & "." & Format$(.Build, "0###") & "." & Format$(.Revision, "0###")
				End With
				AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal)
				oWindowsPrincipal = CType(System.Threading.Thread.CurrentPrincipal, WindowsPrincipal)
				oWindowsIdentity = (CType(oWindowsPrincipal.Identity, WindowsIdentity))
				gsWindowsUserID = CType(GetUser(oWindowsIdentity.Name), String)
			Catch oException As Exception
				WriteEventTrace("Global Initialization Error: " & oException.Message, "StartUp", EventLogEntryType.Error)
				End
			End Try

			Try
				With oInitializeApplication
					If Not .GetSettings(AddBackslash(gsAppPath) & "Aetna EMR Interface.ini") Then
						WriteEventTrace("Invalid or missing start-up file.", "StartUp", EventLogEntryType.Error)
						End
					End If
					gsLogPath = AddBackslash(.LogPath)
					gbTrace = .Trace
					giServicePauseTime = .ServicePauseTime
					galEMailRecipient = .EMailRecipient
					gsEMailSender = .EMailSender
					gbSendEMail = .SendEMail
					gbStatusEMail = .StatusEMail
					galStatusEMailTime = .StatusEMailTime
					galProcessingTime = .ProcessingTime
					gsAetnaDestination = .AetnaDestination
					gsAetnaClientID = .AetnaClientID
					gsAetnaClearFileName = .AetnaClearFileName
					gsAetnaBiometricsFileName = .AetnaBiometricsFileName
					giAetnaProcessingDays = .AetnaProcessingDays
				End With
			Catch oException As Exception
				WriteEventTrace("INI Settings Error: " & vbCrLf & oException.Message, "StartUp", EventLogEntryType.Error)
				End
			Finally
			End Try

			' check application version against the DB version(s)
			If Not oInitializeApplication.VersionCheck(gsVersion) Then
				WriteEventTrace("Error: The version of this service (" & gsVersion & ") doesn't match the database version.", "StartUp", EventLogEntryType.Error)
				End
			End If
			oInitializeApplication = Nothing

			WriteEventTrace("Initialization Complete: Processing!", "StartUp", EventLogEntryType.Information)
			WriteEventLog("For further service initialization information see the service log." & vbCrLf & "Log File: " & gsLogPath & gsLogFileName, "StartUp", EventLogEntryType.Information)
			Return True
		End Get
	End Property
#End Region
End Class
