#Region "Init"
Option Explicit On
Option Strict On
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Text
Imports System.Text.RegularExpressions
#End Region

Public Class Message
#Region "Declarations"
	Private msSender, msRecipient, msSubject, msMessage, msLogoPicture01, msBCC As String
	Private masAttachments() As String = Nothing

#End Region

#Region "Class Properties"
	Public ReadOnly Property Sender(ByVal Address As String) As Boolean
		Get
			msSender = vbNullString
			If Len(Address) > 0 AndAlso ValidEmailAddress(Address) Then
				msSender = Address
				Return True
			Else
				msSender = vbNullString
				Return False
			End If
		End Get
	End Property

	Public ReadOnly Property Recipient(ByVal Address As String) As Boolean
		Get
			Try
				If Not ValidEmailAddress(Address) Then Throw New Exception("Invalid recipient email address.")
				If Address = vbNullString Then
					msRecipient = vbNullString
					Return True
				End If
				If msRecipient <> vbNullString Then
					msRecipient = msRecipient & "; "
				End If
				msRecipient = msRecipient & Address
				Recipient = True
			Catch oException As Exception
				msRecipient = vbNullString
				ErrorMessage = "Invalid recipient email address."
				Recipient = False
			Finally
			End Try
		End Get
	End Property

	Public ReadOnly Property BCC(ByVal Address As String) As Boolean
		Get
			Try
				If Not ValidEmailAddress(Address) Then Throw New Exception("Invalid bcc email address.")
				If Address = vbNullString Then
					msBCC = vbNullString
					Return True
				End If
				If msBCC <> vbNullString Then
					msBCC = msBCC & "; "
				End If
				msBCC = msBCC & Address
				BCC = True
			Catch oException As Exception
				msBCC = vbNullString
				ErrorMessage = "Invalid recipient email address."
				BCC = False
			Finally
			End Try
		End Get
	End Property

	Public ReadOnly Property Subject(ByVal SubjectText As String) As Boolean
		Get
			Try
				msSubject = SubjectText
				Subject = True
			Catch oException As Exception
				ErrorMessage = oException.Message
				Subject = False
			Finally
			End Try
		End Get
	End Property

	Public ReadOnly Property Message(ByVal MessageText As String) As Boolean
		Get
			Try
				msMessage = msMessage & MessageText
				Message = True
			Catch oException As Exception
				ErrorMessage = oException.Message
				Return False
			Finally
			End Try
		End Get
	End Property

	Public WriteOnly Property Logo01PathFileName() As String
		Set(value As String)
			Dim sPathFileName As String = SetString(value)
			Dim oFileInfo As New IO.FileInfo(sPathFileName)

			Try
				If oFileInfo.Exists Then
					msLogoPicture01 = sPathFileName
				Else
					msLogoPicture01 = String.Empty
				End If
			Catch oException As Exception
				ErrorMessage = oException.Message
			Finally
			End Try
		End Set
	End Property

	Public ReadOnly Property AddAttachment(ByVal PathFileName As String) As Boolean
		Get
			Dim sPathFileName As String = SetString(PathFileName)
			Dim oFileInfo As New IO.FileInfo(sPathFileName)

			Try
				If sPathFileName = vbNullString Then Return True
				If Not oFileInfo.Exists Then Throw New Exception("Invalid Attachment: " & sPathFileName)
				If masAttachments Is Nothing Then
					ReDim masAttachments(0)
				Else
					ReDim Preserve masAttachments(UBound(masAttachments, 1) + 1)
				End If
				masAttachments(UBound(masAttachments, 1)) = sPathFileName
				AddAttachment = True
			Catch oException As Exception
				ErrorMessage = oException.Message
				AddAttachment = False
			Finally
			End Try
		End Get
	End Property

	Public Property ErrorMessage As String
#End Region

#Region "Routines"
  Public Sub New()
    Initialize()
  End Sub

	Public Sub Initialize()
		msSender = String.Empty
		msRecipient = String.Empty
		msBCC = String.Empty
		msSubject = String.Empty
		msMessage = String.Empty
		msLogoPicture01 = String.Empty
		ErrorMessage = String.Empty
	End Sub

	Private Function ValidEmailAddress(ByVal EmailAddress As String) As Boolean
    Dim oRegex As New Regex("\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}")

    ValidEmailAddress = oRegex.IsMatch(EmailAddress)
  End Function

	Private Function AttachmentType(ByVal FileName As String) As String
		Dim sFileName As String = SetString(FileName)

		Try
			If Len(sFileName) < 3 Then
				AttachmentType = vbNullString
			Else
				AttachmentType = LCase$(SetString(Mid$(sFileName, Len(sFileName) - 2, 3)))
			End If
		Catch oException As Exception
			ErrorMessage = oException.Message
			AttachmentType = vbNullString
		Finally
		End Try
	End Function

	Private Function ConvertToText(Message As String) As String
		ConvertToText = Message
		ConvertToText = ConvertToText.Replace("&nbsp;", Space(1))
		ConvertToText = ConvertToText.Replace("<br />", vbCrLf)
		ConvertToText = ConvertToText.Replace("&bull;", Chr(149))
		ConvertToText = ConvertToText.Replace("<font size=" & gsDoubleQuote & "1" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("<font size=" & gsDoubleQuote & "2" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("<font size=" & gsDoubleQuote & "3" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("<font size=" & gsDoubleQuote & "4" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("<font size=" & gsDoubleQuote & "5" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("<font size=" & gsDoubleQuote & "6" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("<font size=" & gsDoubleQuote & "7" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("<font color=" & gsDoubleQuote & "#00ABC2" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("<font color=" & gsDoubleQuote & "#E3502D" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("<font color=" & gsDoubleQuote & "#777777" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("<font color=" & gsDoubleQuote & "#000000" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("<font color=" & gsDoubleQuote & "#FFFFFF" & gsDoubleQuote & ">", vbNullString)
		ConvertToText = ConvertToText.Replace("</font>", vbNullString)
	End Function

	Private Sub LinkImage(CID As String, ImageFile As String, MediaType As String, View As AlternateView)
    Dim oLinkedResource As LinkedResource

		If InStr(1, msMessage, CID) > 0 AndAlso ImageFile.Length > 0 Then
			oLinkedResource = New LinkedResource(ImageFile, MediaType)
			oLinkedResource.ContentId = CID
			oLinkedResource.ContentType.MediaType = MediaType
			oLinkedResource.TransferEncoding = TransferEncoding.Base64
			oLinkedResource.ContentType.Name = oLinkedResource.ContentId
			oLinkedResource.ContentLink = New Uri("cid:" + oLinkedResource.ContentId)
			View.LinkedResources.Add(oLinkedResource)
		End If
  End Sub

	Private Function AddImageTag(CID As String, ImageTag As String) As String
		AddImageTag = CID
		If Len(ImageTag) > 0 AndAlso Len(CID) > 2 Then AddImageTag = Mid(CID, 1, Len(CID) - 2) & ImageTag & Mid(CID, Len(CID) - 1, 2)
	End Function
#End Region

#Region "Send"
	Public ReadOnly Property Send(Optional ImageTag As String = vbNullString) As Boolean
		Get
			Dim oSMTPClient As New SmtpClient(gsSMTPAddress)
			Dim iWork As Int32
			Dim oMailMessage As New MailMessage
			Dim oPlainView As AlternateView
			Dim oHTMLView As AlternateView
			Dim sTextMessage As String
			Dim sMediaType As String = MediaTypeNames.Image.Jpeg '"image/png"

			Try
				If msRecipient = vbNullString Then Throw New Exception("Missing recipient(s), send cancled.")
				If msSender = vbNullString Then Throw New Exception("Missing sender, send cancled.")
				oMailMessage = New MailMessage(New MailAddress(msSender), New MailAddress(msRecipient))

				With oMailMessage
					If msBCC.Length > 0 Then .Bcc.Add(msBCC)
					.Subject = msSubject
					sTextMessage = ConvertToText(msMessage)
					oPlainView = AlternateView.CreateAlternateViewFromString(sTextMessage, Encoding.UTF8, MediaTypeNames.Text.Plain)
					oHTMLView = AlternateView.CreateAlternateViewFromString("<html><body>" & msMessage & "</body></html>", Encoding.UTF8, MediaTypeNames.Text.Html)
					LinkImage(AddImageTag("logo01id", ImageTag), msLogoPicture01, MediaTypeNames.Image.Jpeg, oHTMLView)
					.Priority = MailPriority.Normal
					If Not masAttachments Is Nothing Then
						If UBound(masAttachments, 1) > -1 Then
							For iWork = LBound(masAttachments, 1) To UBound(masAttachments, 1)
								Select Case AttachmentType(masAttachments(iWork))
									Case "pdf"
										.Attachments.Add(New Attachment(masAttachments(iWork), MediaTypeNames.Application.Pdf))
									Case "rtf"
										.Attachments.Add(New Attachment(masAttachments(iWork), MediaTypeNames.Application.Rtf))
									Case "zip"
										.Attachments.Add(New Attachment(masAttachments(iWork), MediaTypeNames.Application.Zip))
									Case Else
										.Attachments.Add(New Attachment(masAttachments(iWork), MediaTypeNames.Application.Octet))
								End Select
							Next iWork
						End If
					End If
					.AlternateViews.Add(oPlainView)
					.AlternateViews.Add(oHTMLView)
					.IsBodyHtml = True
				End With
				oSMTPClient.Send(oMailMessage)
				Send = True
			Catch oException As Exception
				ErrorMessage = oException.Message
				Send = False
			Finally
				oMailMessage.Dispose()
				oMailMessage = Nothing
				oSMTPClient = Nothing
			End Try
		End Get
	End Property
#End Region

#Region "Signoff"
  Public ReadOnly Property Signoff() As Boolean
    Get
      Try
        ErrorMessage = String.Empty
        Initialize()
        Signoff = True
      Catch oException As Exception
        ErrorMessage = oException.Message
        Signoff = False
      Finally
      End Try
    End Get
  End Property
#End Region
End Class
