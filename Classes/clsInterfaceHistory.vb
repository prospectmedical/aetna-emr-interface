﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class InterfaceHistory
#Region "Declartions"
  Private miInterfaceHistoryID As Int32
  Private msErrorMessage As String
#End Region

#Region "Class Properties"
  Public ReadOnly Property InterfaceHistoryID() As Int32
    Get
      Return miInterfaceHistoryID
    End Get
  End Property

  Public ReadOnly Property ErrorMessage As String
    Get
      Return msErrorMessage
    End Get
  End Property
#End Region

#Region "MarkStart"
  Public Function MarkStart(InterfaceTypeID As Int32, StartDateTime As Date) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    MarkStart = False
    Try
      If Not OpenDB(oDBC) Then Throw New Exception("InterfaceHistory.MarkStart: Error opening the database.")
      oCmd.CommandText = "iInterfaceHistory"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@StartDateTime", OleDbType.Date).Value = StartDateTime
      oCmd.Parameters.Add("@AccountID", OleDbType.BigInt).Value = giAccountID
      oCmd.Parameters.Add("@InterfaceTypeID", OleDbType.BigInt).Value = InterfaceTypeID
      oCmd.Parameters.Add("@NewID", OleDbType.BigInt).Direction = ParameterDirection.Output
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      miInterfaceHistoryID = SetInt32(oCmd.Parameters("@NewID").Value)
      MarkStart = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      MarkStart = False
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region

#Region "WriteFileProcessing"
  Public Function WriteFileProcessing(InterfaceHistoryID As Int32, PathFileName As String, StartDate As String, EndDate As String, SurveyCount As Int32) As Int32
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    Try
      If Not OpenDB(oDBC) Then Throw New Exception("Error opening the database.")
      oCmd.CommandText = "iFileProcessing"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@InterfaceHistoryID", OleDbType.BigInt).Value = InterfaceHistoryID
      oCmd.Parameters.Add("@FileName", OleDbType.VarChar, 200).Value = PathFileName
      oCmd.Parameters.Add("@StartDate", OleDbType.VarChar, 10).Value = StartDate
      oCmd.Parameters.Add("@EndDate", OleDbType.VarChar, 10).Value = EndDate
      oCmd.Parameters.Add("@SurveyCount", OleDbType.BigInt).Value = SurveyCount
      oCmd.Parameters.Add("@NewID", OleDbType.BigInt).Direction = ParameterDirection.Output
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      WriteFileProcessing = SetInt32(oCmd.Parameters("@NewID").Value)
    Catch oException As Exception
      msErrorMessage = "InterfaceHistory.FileProcessing: " & oException.Message
      WriteFileProcessing = 0
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region

#Region "MarkHANCreate"
  Public Function MarkFileCreate(PathFileName As String, RecordsRead As Int32, RecordsWritten As Int32) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    MarkFileCreate = False
    Try
      If Not OpenDB(oDBC) Then Throw New Exception("Error opening the database.")
      oCmd.CommandText = "uInterfaceHistoryFileCreate"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@InterfaceHistoryID", OleDbType.BigInt).Value = InterfaceHistoryID
      oCmd.Parameters.Add("@PathFileName", OleDbType.VarChar, 200).Value = PathFileName
      oCmd.Parameters.Add("@RecordsRead", OleDbType.BigInt).Value = RecordsRead
      oCmd.Parameters.Add("@RecordsWritten", OleDbType.BigInt).Value = RecordsWritten
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      MarkFileCreate = True
    Catch oException As Exception
      msErrorMessage = "InterfaceHistory.MarkFileCreate: " & oException.Message
      MarkFileCreate = False
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region

#Region "MarkEnd"
  Public Function MarkEnd(FilesProcessed As Int32, EndDateTime As Date) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand

    MarkEnd = False
    Try
      If Not OpenDB(oDBC) Then Throw New Exception("InterfaceHistory.MarkEnd: Error opening the database.")
      oCmd.CommandText = "uInterfaceHistory"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@InterfaceHistoryID", OleDbType.BigInt).Value = miInterfaceHistoryID
      oCmd.Parameters.Add("@FilesProcessed", OleDbType.BigInt).Value = FilesProcessed
      oCmd.Parameters.Add("@EndDateTime", OleDbType.Date).Value = EndDateTime
      oCmd.Connection = oDBC
      oCmd.ExecuteNonQuery()
      MarkEnd = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      MarkEnd = False
    Finally
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region
End Class

