#Region "Init"
Option Strict On
Option Explicit On
#End Region

Module [Global]
#Region "Variables"
	Public gsAppPath, gsWindowsUserID, gsVersion, gsLogPath, gsEMailSender, gsAetnaDestination, gsAetnaClientID, gsAetnaClearFileName, gsAetnaBiometricsFileName As String
	Public gsLogFileName, gsEventLogName, gsHTMLPath, gsGraphicsPath, gsEMRServer, gsEMRDatabase As String
	Public giServicePauseTime, giAetnaProcessingDays As Int32
	Public gbTrace, gbSendEMail, gbStatusEMail As Boolean
	Public galProcessingTime As ArrayList
	Public galEMailRecipient, galStatusEMailTime As ArrayList
#End Region

#Region "Constants"
	Public Const gsApplicationName As String = "Aetna EMR Interface"
	Public Const gsEntityDescription As String = "Prospect Crozer Health System, Inc."
	Public Const giEMailPauseTime As Int32 = 2000
  Public Const gsSMTPAddress As String = "smtp.crozer.org"
	Public Const giAccountID As Int32 = 1
	Public Const giSQLTimeOutNormal As Int32 = 30
	Public Const giSQLTimeOutExtended As Int32 = 480
	Public Const giProcessingDays As Int32 = 7
	Public Const gsDoubleQuote As String = Chr(34)
	Public Const gsBCC As String = ""
#End Region
End Module