﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Threading
Imports System.IO
#End Region

Module EMailRoutines
#Region "Routines"
  Private Function StartUpEMailMessage() As String
    StartUpEMailMessage = "The " & gsApplicationName & " Service has been started." & "<br />"
    StartUpEMailMessage = StartUpEMailMessage & "__________________________________________________________________________________________"
    StartUpEMailMessage = StartUpEMailMessage & "<br />" & "<br />"
    StartUpEMailMessage = StartUpEMailMessage & "END OF REPORT" & "<br />" & "<br />" & Disclaimer() & "<br />"
  End Function

  Private Function ShutDownEMailMessage() As String
    ShutDownEMailMessage = "The " & gsApplicationName & " Service has been shutdown." & "<br />"
    ShutDownEMailMessage = ShutDownEMailMessage & "__________________________________________________________________________________________"
    ShutDownEMailMessage = ShutDownEMailMessage & "<br />" & "<br />"
    ShutDownEMailMessage = ShutDownEMailMessage & "See attached service log." & "<br />" & "END OF REPORT" & "<br />" & "<br />" & Disclaimer() & "<br />"
  End Function

  Private Function MessageBody(ByVal Message As String) As String
    MessageBody = Message & "<br />"
    MessageBody = MessageBody & "__________________________________________________________________________________________"
    MessageBody = MessageBody & "<br />" & "<br />"
    MessageBody = MessageBody & "END OF REPORT" & "<br />" & "<br />" & Disclaimer() & "<br />"
  End Function

  Private Function Disclaimer() As String
    Return "***** NOTE: Do not reply to this email!" & "<br />" & StringRepeat("&nbsp;", 24) & "This email address is not monitored." & "<br />"
  End Function
#End Region

#Region "StartUpEMail"
	Public Sub StartUpEMail()
		Dim sRoutine As String = "StartUpEMail"
		Dim sBodyText, sInsertText As String
		Dim iWork As Int32
		Dim dtCurrentDateTime As Date = Now
		Dim oEMailFunctions As New EMailFunctions
		Dim oStreamReader As StreamReader = Nothing
		Dim sImageTag As String = Format$(Now, "yyyyMMddHHmmss")

		Try
			TraceWrite("Start Send Startup eMail", sRoutine)
			If gbSendEMail Then
				If galEMailRecipient.Count > 0 Then
					oStreamReader = New StreamReader(gsHTMLPath & "Start01.html")
					sBodyText = oStreamReader.ReadToEnd
					CloseStreamReader(oStreamReader)
					sInsertText = gsApplicationName & " Startup"
					sInsertText = sInsertText & ASPCRLF(2) & ASPSpace(5) & "Status: Started on " & MonthName(Month(dtCurrentDateTime)) & " " & Day(dtCurrentDateTime) & ", " & Year(dtCurrentDateTime) & " at " & Format(dtCurrentDateTime, "HH:mm")
					sBodyText = sBodyText.Replace("[text01]", sInsertText)
					sBodyText = sBodyText.Replace("[donotreply01]", oEMailFunctions.DoNoTReply)
					sBodyText = sBodyText.Replace("[imagetag]", sImageTag)
					For iWork = 0 To galEMailRecipient.Count - 1
						oEMailFunctions.Logo01PathFileName = gsGraphicsPath & "Logo01.png"
						If Not oEMailFunctions.Send(galEMailRecipient.Item(iWork).ToString, gsApplicationName & " Startup", sBodyText, False, sImageTag) Then
							TraceWrite("Send startup eMail failed: " & oEMailFunctions.ErrorMessage, sRoutine)
						End If
					Next iWork
					TraceWrite("End Send Startup eMail", sRoutine)
				End If
			End If
		Catch oException As Exception
			TraceWrite("Send startup eMail failed: " & oException.Message, sRoutine)
		Finally
		End Try
	End Sub
#End Region

#Region "DailyStatusEMail"
	Public Sub DailyStatusEMail()
		Dim sRoutine As String = "DailyStatusEMail"
		Dim sBodyText, sInsertText As String
		Dim iWork, iWork1 As Int32
		Dim dtCurrentDateTime As Date = Now
		Dim sCurrentDate As String = Format$(dtCurrentDateTime, "MM/dd/yyyy")
		Dim oProcessed As New Processed
		Dim oEMailFunctions As New EMailFunctions
		Dim oStreamReader As StreamReader = Nothing
		Dim sImageTag As String = Format$(Now, "yyyyMMddHHmmss")

		Try
			If galStatusEMailTime.Count > 0 Then
				For iWork = 0 To galStatusEMailTime.Count - 1
					If CDate(sCurrentDate & " " & galStatusEMailTime.Item(iWork).ToString) <= Now Then
						If Not oProcessed.Completed(1, sCurrentDate, galStatusEMailTime.Item(iWork).ToString) Then
							TraceWrite("Start Send Status eMail: " & sCurrentDate & " " & galStatusEMailTime.Item(iWork).ToString, sRoutine)
							If gbSendEMail Then
								If galEMailRecipient.Count > 0 Then
									oStreamReader = New StreamReader(gsHTMLPath & "Status01.html")
									sBodyText = oStreamReader.ReadToEnd
									CloseStreamReader(oStreamReader)
									sInsertText = gsApplicationName & " Status"
									sInsertText = sInsertText & ASPCRLF(2) & ASPSpace(5) & "Status: Available as of " & MonthName(Month(dtCurrentDateTime)) & " " & Day(dtCurrentDateTime) & ", " & Year(dtCurrentDateTime) & " at " & Format(dtCurrentDateTime, "HH:mm")
									sBodyText = sBodyText.Replace("[text01]", sInsertText)
									sBodyText = sBodyText.Replace("[donotreply01]", oEMailFunctions.DoNoTReply)
									sBodyText = sBodyText.Replace("[imagetag]", sImageTag)
									For iWork1 = 0 To galEMailRecipient.Count - 1
										oEMailFunctions.Logo01PathFileName = gsGraphicsPath & "Logo01.png"
										If Not oEMailFunctions.Send(galEMailRecipient.Item(iWork1).ToString, gsApplicationName & " Status", sBodyText, False, sImageTag) Then
											TraceWrite("Send status eMail failed: " & oEMailFunctions.ErrorMessage, sRoutine)
										End If
									Next iWork1
								End If
								oProcessed.Insert(1, sCurrentDate, galStatusEMailTime.Item(iWork).ToString)
								TraceWrite("End Send Status eMail: " & sCurrentDate & " " & galStatusEMailTime.Item(iWork).ToString, sRoutine)
							End If
						End If
					End If
				Next
			End If
		Catch oException As Exception
			TraceWrite("Send status eMail failed: " & oException.Message, sRoutine)
		Finally
			oProcessed = Nothing
		End Try
	End Sub
#End Region

#Region "ShutdownEMail"
	Public Sub ShutdownEMail()
		Dim sRoutine As String = "ShutdownEMail"
		Dim sBodyText, sInsertText As String
		Dim iWork As Int32
		Dim dtCurrentDateTime As Date = Now
		Dim oEMailFunctions As New EMailFunctions
		Dim oStreamReader As StreamReader = Nothing
		Dim sImageTag As String = Format$(Now, "yyyyMMddHHmmss")

		Try
			TraceWrite("Start Send Shutdown eMail", sRoutine)
			If gbSendEMail Then
				If galEMailRecipient.Count > 0 Then
					oStreamReader = New StreamReader(gsHTMLPath & "Shutdown01.html")
					sBodyText = oStreamReader.ReadToEnd
					CloseStreamReader(oStreamReader)
					sInsertText = gsApplicationName & " Shutdown"
					sInsertText = sInsertText & ASPCRLF(2) & ASPSpace(5) & "Status: Shutdown on " & MonthName(Month(dtCurrentDateTime)) & " " & Day(dtCurrentDateTime) & ", " & Year(dtCurrentDateTime) & " at " & Format(dtCurrentDateTime, "HH:mm")
					sBodyText = sBodyText.Replace("[text01]", sInsertText)
					sBodyText = sBodyText.Replace("[donotreply01]", oEMailFunctions.DoNoTReply)
					sBodyText = sBodyText.Replace("[imagetag]", sImageTag)
					For iWork = 0 To galEMailRecipient.Count - 1
						oEMailFunctions.Logo01PathFileName = gsGraphicsPath & "Logo01.png"
						If Not oEMailFunctions.Send(galEMailRecipient.Item(iWork).ToString, gsApplicationName & " Shutdown", sBodyText, False, sImageTag) Then
							TraceWrite("Send shutdown eMail failed: " & oEMailFunctions.ErrorMessage, sRoutine)
						End If
					Next iWork
					TraceWrite("End Send Shutdown eMail", sRoutine)
				End If
			End If
		Catch oException As Exception
			TraceWrite("Send shutdown eMail failed: " & oException.Message, sRoutine)
		Finally
		End Try
	End Sub
#End Region

#Region "SendEMail"
	Public Sub SendEMail(ByVal Subject As String, ByVal Message As String, Recipients As String)
    Dim oSMTPEMail As SMTPEMail.Mail
    Dim sRecipient As String
    Dim saRecipients As String()

    Try
      saRecipients = Recipients.Split(New Char() {","c})
      For Each sRecipient In saRecipients
        oSMTPEMail = New SMTPEMail.Mail
        If Not oSMTPEMail.Recipient(sRecipient) Then Throw New Exception("Recipient EMail Error: " & oSMTPEMail.ErrorMessage)
        If Not oSMTPEMail.Sender(gsEMailSender) Then Throw New Exception("Sender EMail Error: " & oSMTPEMail.ErrorMessage)
        If Not oSMTPEMail.Subject(Subject) Then Throw New Exception("Subject EMail Error: " & oSMTPEMail.ErrorMessage)
        If Not oSMTPEMail.Message(MessageBody(Message)) Then Throw New Exception("Message EMail Error: " & oSMTPEMail.ErrorMessage)
        If Not oSMTPEMail.Send(gsSMTPAddress) Then Throw New Exception("Send EMail Error: " & oSMTPEMail.ErrorMessage)
        Thread.Sleep(giEMailPauseTime)
      Next
    Catch oException As Exception
      WriteEventTrace(oException.Message, "SendEMail", EventLogEntryType.Error)
    Finally
      oSMTPEMail = Nothing
    End Try
  End Sub
#End Region
End Module
