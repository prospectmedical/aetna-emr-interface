﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
Imports System.Threading
#End Region

Public Class EMailFunctions
#Region "Class Properties"
	Public Property Logo01PathFileName As String
	Public Property Attachment01PathFileName As String
	Public Property ErrorMessage As String
#End Region

#Region "Routines"
	Public Sub New()
		Initialize()
	End Sub

	Private Sub Initialize()
		Logo01PathFileName = String.Empty
		Attachment01PathFileName = String.Empty
		ErrorMessage = String.Empty
	End Sub

	'Public Function FormatContent(AccountID As Int32, Content As String) As String
	'	FormatContent = Content
	'	FormatContent = FormatContent.Replace("[firstname]", FirstName(AccountID))
	'	FormatContent = FormatContent.Replace("[companyname]", CompanyName(AccountID))
	'	FormatContent = FormatContent.Replace("[activationcode]", ActivationCode(AccountID))
	'	FormatContent = FormatContent.Replace("[sitename]", gsSiteName)
	'	FormatContent = FormatContent.Replace("[accountid]", AccountID.ToString)
	'End Function
#End Region

#Region "DoNoTReply"
	Public Function DoNoTReply() As String
		Return ASPCRLF(5) & "***** NOTE: Do not reply to this e-mail!" & ASPCRLF(1) & ASPSpace(23) & "This Email address is not monitored." & ASPCRLF(1)
	End Function
#End Region

#Region "Send"
	Public ReadOnly Property Send(Recipient As String, Subject As String, EMailMessage As String, IncludeBCC As Boolean, Optional ImageTag As String = vbNullString) As Boolean
		Get
			Dim oMessage As New Message
			Dim sEMailMessage As String = EMailMessage
			Dim sErrorMessage As String = vbNullString
			Dim bErrorFlag As Boolean = False


			If Not gbSendEMail Then Return True
			Send = False
			Try
				If Not oMessage.Sender(gsEMailSender) Then Throw New Exception("EMailFunctions.Send.Sender(" & gsEMailSender & "): " & oMessage.ErrorMessage)
				If Not oMessage.Recipient(Recipient) Then Throw New Exception("EMailFunctions.Send.Recipient(" & Recipient & "): " & oMessage.ErrorMessage)
				If IncludeBCC And gsBCC <> vbNullString Then
					If Not oMessage.BCC(gsBCC) Then Throw New Exception("EMailFunctions.Send.gsbcc(" & gsBCC & "): " & oMessage.ErrorMessage)
				End If
				If Not oMessage.Subject(Subject) Then Throw New Exception("EMailFunctions.Send.Subject(" & Subject & "): " & oMessage.ErrorMessage)
				If Not oMessage.Message(sEMailMessage) Then Throw New Exception("EMailFunctions.Send.Message(" & sEMailMessage & "): " & oMessage.ErrorMessage)
				If Len(Attachment01PathFileName) > 0 Then
					If Not oMessage.AddAttachment(Attachment01PathFileName) Then Throw New Exception("EMailFunctions.Send.Add Attachment 01(" & Attachment01PathFileName & "): " & oMessage.ErrorMessage)
				End If
				If Logo01PathFileName.Length > 0 Then oMessage.Logo01PathFileName = Logo01PathFileName
				If Not oMessage.Send(ImageTag) Then Throw New Exception("EMailFunctions.Send.Send: " & oMessage.ErrorMessage)
				If Not oMessage.Signoff Then Throw New Exception("EMailFunctions.Send.Signoff: " & oMessage.ErrorMessage)
				Thread.Sleep(giEMailPauseTime)
				Send = True
			Catch oException As Exception
				ErrorMessage = oException.Message
				sErrorMessage = ErrorMessage
				bErrorFlag = True
			Finally
			End Try
		End Get
	End Property
#End Region
End Class
