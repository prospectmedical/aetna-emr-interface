#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
Imports System.IO
Imports System.Xml
Imports System.Text.RegularExpressions
#End Region

Module ServiceRoutines
#Region "Declarations"
  Private msErrorMessage As String
  Private miSequenceNumber As Int32
	Private moInterfaceHistory As New InterfaceHistory
#End Region

#Region "Routines"
	Private Function ValidEmail(EmailAddress As String) As Boolean
    Dim sPattern As String = "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"

    Return Regex.IsMatch(EmailAddress, sPattern)
  End Function
  Private Sub AddField(ByRef OutRecord As String, Field As String, MaximumLength As Int32)
    Dim sField As String = Field

    If MaximumLength > 0 Then
      If Len(Field) > MaximumLength Then Field = Mid(Field, 1, MaximumLength)
    End If
    If Len(OutRecord) > 0 Then OutRecord = OutRecord & "|"
    OutRecord = OutRecord & sField
  End Sub

  Private Function ProcessResults(FileToProcess As String) As Boolean
    Dim sRoutine As String = "ProcessResults"
    Dim oSurveyResults As New SurveyResults
    Dim oXmlDocument As New XmlDocument
    Dim oXmlNodeList As XmlNodeList
    Dim oXmlNode As XmlNode
    Dim sStartDate As String = vbNullString
    Dim sEndDate As String = vbNullString
    Dim iSurveyCount, iFileProcessingID As Int32
    Try
      oXmlDocument.Load(FileToProcess)
      oXmlNodeList = oXmlDocument.SelectNodes("DATA_EXPORT/HEADER/RECDATE")
      For Each oXmlNode In oXmlNodeList
        sStartDate = oXmlNode.ChildNodes.Item(0).InnerText
        sEndDate = oXmlNode.ChildNodes.Item(1).InnerText
      Next

      oXmlNodeList = oXmlDocument.SelectNodes("DATA_EXPORT/HEADER")
      For Each oXmlNode In oXmlNodeList
        iSurveyCount = SetInt32(oXmlNode.ChildNodes.Item(1).InnerText)
      Next

			iFileProcessingID = moInterfaceHistory.WriteFileProcessing(moInterfaceHistory.InterfaceHistoryID, gsAetnaDestination & FileToProcess, sStartDate, sEndDate, iSurveyCount)

			oXmlNodeList = oXmlDocument.SelectNodes("DATA_EXPORT/HEADER/QUESTION_MAP/QUESTION")
      For Each oXmlNode In oXmlNodeList
        If Not oSurveyResults.UpdateQuestion(iFileProcessingID, oXmlNode.ChildNodes.Item(0).InnerText, oXmlNode.ChildNodes.Item(1).InnerText, oXmlNode.ChildNodes.Item(2).InnerText) Then Throw New Exception(oSurveyResults.ErrorMessage)
      Next

      ProcessResults = True
    Catch oException As Exception
      TraceWrite("Error: " & oException.Message, sRoutine)
      ProcessResults = False
    Finally
    End Try
  End Function

	Private Function MessageID() As String
		Return Right(System.Guid.NewGuid.ToString, 10) & Format$(Now, "HHmmssffff")
	End Function

	Private Function FormatData(InField As String, Type As Int32) As String
    Dim sWork As String
    Dim sWork1 As String = vbNullString
    Dim iWork As Int32

    If Len(InField) <= 0 Then Return vbNullString
    Select Case Type
      Case 1 'no dashes or parens
        sWork = Trim$(LCase$(InField.Replace("(", vbNullString).Replace(")", vbNullString).Replace("-", vbNullString).Replace(Space$(1), vbNullString)))
        iWork = InStr(1, sWork, "x")
        If iWork > 0 Then
          If iWork = 1 Then
            sWork = vbNullString
          Else
            sWork = Mid$(sWork, 1, iWork - 1)
          End If
        End If
        If Len(sWork) > 0 Then
          For iWork = 1 To Len(sWork)
            If Asc(Mid$(sWork, iWork, 1)) >= 48 AndAlso Asc(Mid$(sWork, iWork, 1)) <= 57 Then sWork1 = sWork1 & Mid$(sWork, iWork, 1)
          Next
        Else
          sWork1 = vbNullString
        End If
        Return sWork1
      Case 2 'date yyyymmdd
        If IsDate(InField) Then
          Return Format$(CDate(InField), "yyyyMMdd")
        Else
          Return vbNullString
        End If
      Case 3 'gender
        sWork = UCase$(InField)
        If sWork <> "M" AndAlso sWork <> "F" Then sWork = "U"
        Return sWork
      Case 4 'marital status
        sWork = UCase$(InField)
        If sWork <> "A" AndAlso sWork <> "D" AndAlso sWork <> "M" AndAlso sWork <> "S" AndAlso sWork <> "W" Then sWork = "U"
        Return sWork
      Case 5 'icd9
        If Mid$(LCase$(InField), 1, 4) = "icd-" Then
          If Len(InField) = 4 Then Return vbNullString
          Return Trim$(Mid$(InField, 5, Len(InField)))
        Else
          Return InField
        End If
      Case 6 'date yyyymmddhhmm
        If IsDate(InField) Then
          Return Format$(CDate(InField), "yyyyMMddHHmm")
        Else
          Return vbNullString
        End If
      Case 7 'date yyyymmddhhmmss
        If IsDate(InField) Then
          Return Format$(CDate(InField), "yyyyMMddHHmmss")
        Else
          Return vbNullString
        End If
      Case Else
        Return vbNullString
    End Select
  End Function
#End Region

#Region "StartService"
  Public Sub StartService(InTextWriterTraceListener As TextWriterTraceListener)
    Dim sRoutine As String = "StartService"
    Dim oFile As StreamWriter = Nothing
    Dim oServicesToRun() As System.ServiceProcess.ServiceBase
    Dim oStartup As New StartUp

    WriteEventLog("Service main method starting at " & Format$(Now, "MM/dd/yyyy HH:mm"), sRoutine, EventLogEntryType.Information)
    If oStartup.Run Then
      CreateDirectory(gsLogPath)
      gsLogFileName = gsApplicationName & " Log " & Format$(Now, "yyyyMMdd HHmmss") & ".txt"
      oFile = File.CreateText(gsLogPath & gsLogFileName)
      InTextWriterTraceListener = New TextWriterTraceListener(oFile)
      Trace.Listeners.Add(InTextWriterTraceListener)
      Trace.AutoFlush = True
      TraceWrite("Startup.Run was initialized...", sRoutine)
      TraceWrite("Application Name: " & gsApplicationName, sRoutine)
      TraceWrite("Windows UserID: " & gsWindowsUserID, sRoutine)
      TraceWrite("Entity Description: " & gsEntityDescription, sRoutine)
      TraceWrite("Application Version: " & gsVersion, sRoutine)
      TraceWrite("Application Startup Path: " & gsAppPath, sRoutine)
			TraceWrite("Service Pause Time: " & Format$(giServicePauseTime \ 60000, "#,##0") & " minutes", sRoutine)
			If galEMailRecipient.Count > 0 Then
				For iWork = 0 To galEMailRecipient.Count - 1
					TraceWrite("EMail Recipient(" & Format$(iWork + 1, "#,##0") & "): " & galEMailRecipient.Item(iWork).ToString, sRoutine)
				Next iWork
			End If
			TraceWrite("EMail Sender: " & gsEMailSender, sRoutine)
			If gbSendEMail Then
				TraceWrite("Send EMail: True", sRoutine)
			Else
				TraceWrite("Send EMail: False", sRoutine)
			End If
			If gbStatusEMail Then
				TraceWrite("Send Status EMail: True", sRoutine)
			Else
				TraceWrite("Send Status EMail: False", sRoutine)
			End If
			If galStatusEMailTime.Count > 0 Then
				For iWork = 0 To galStatusEMailTime.Count - 1
					TraceWrite("Status eMail Time(" & Format$(iWork + 1, "#,##0") & "): " & galStatusEMailTime.Item(iWork).ToString, sRoutine)
				Next iWork
			End If
			If galProcessingTime.Count > 0 Then
				For iWork = 0 To galProcessingTime.Count - 1
					TraceWrite("Processing Time(" & Format$(iWork + 1, "#,##0") & "): " & galProcessingTime.Item(iWork).ToString, sRoutine)
				Next iWork
			End If
			TraceWrite("Aetna Destination: " & gsAetnaDestination, sRoutine)
			TraceWrite("Aetna Client ID: " & gsAetnaClientID, sRoutine)
			TraceWrite("Aetna Clear File Name: " & gsAetnaClearFileName, sRoutine)
			TraceWrite("Aetna Biometrics File Name: " & gsAetnaBiometricsFileName, sRoutine)
			TraceWrite("Aetna Processing Days: " & giAetnaProcessingDays, sRoutine)

			If Not gbTrace Then
        gbTrace = True
        TraceWrite("Trace is disabled", sRoutine)
        gbTrace = False
      End If
      WriteEventTrace("Startup Complete", sRoutine, EventLogEntryType.Information)
      TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
      TraceWrite(StringRepeat("-", 100), sRoutine)
      TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
      TraceWrite(Space$(1), sRoutine)
    Else
      WriteEventTrace("Startup.Run failed to initialize...", sRoutine, EventLogEntryType.Error)
      WriteEventTrace(oStartup.ErrorMessage, sRoutine, EventLogEntryType.Error)
      End
    End If
    oStartup = Nothing
    oServicesToRun = New System.ServiceProcess.ServiceBase() {New Aetna}
    System.ServiceProcess.ServiceBase.Run(oServicesToRun)
    WriteEventTrace("Service main method exiting...", sRoutine, EventLogEntryType.Information)
    Trace.Listeners.Remove(InTextWriterTraceListener)
    InTextWriterTraceListener.Close()
    InTextWriterTraceListener = Nothing
    oFile.Close()
  End Sub
#End Region

#Region "Write Encounters"
	Public Sub ProcessEncounterDates()
		Dim sRoutine As String = "ProcessEncounterDates"
		Dim oProcessed As New Processed
		Dim iWork, iWork1 As Int32
		Dim dtCurrentDateTime As Date
		Dim sCurrentDate As String
		Dim iProcessingTypeID As Int32 = 2


		If galProcessingTime.Count > 0 Then
			For iWork = 0 To galProcessingTime.Count - 1
				If CDate(Format$(Now, "MM/dd/yyyy") & " " & galProcessingTime.Item(iWork).ToString) <= Now Then
					If oProcessed.EMRProcessed("daily", "Daily Complete") Then
						For iWork1 = giAetnaProcessingDays To 0 Step -1
							dtCurrentDateTime = DateAdd(DateInterval.Day, Math.Abs(iWork1) * -1, Now)
							sCurrentDate = Format$(dtCurrentDateTime, "MM/dd/yyyy")
							If Not oProcessed.Completed(iProcessingTypeID, sCurrentDate, galProcessingTime.Item(iWork).ToString) Then
								WriteEncounter(dtCurrentDateTime, galProcessingTime.Item(iWork).ToString)
							Else
								TraceWrite("Processing not required for " & Format$(dtCurrentDateTime, "MM/dd/yyyy"), sRoutine)
							End If
						Next iWork1
					Else
						TraceWrite("Waiting for EMR process to complete.", sRoutine)
					End If
				End If
			Next iWork
		End If
	End Sub

	Public Sub WriteEncounter(CurrentDateTime As Date, ProcessingTime As String)
		Dim sRoutine As String = "WriteEncounter"
		Dim sCurrentDate As String
		Dim oProcessed As New Processed
		Dim iProcessingTypeID As Int32 = 2
		Dim dtStartDate, dtEndDate As Date
		Dim dtProcessingDateTime As Date = Now

		Try
			sCurrentDate = Format$(CurrentDateTime, "MM/dd/yyyy")
			dtStartDate = DateAdd(DateInterval.Day, (giProcessingDays - 1) * -1, CDate(Format$(DateAdd(DateInterval.Day, -1, CurrentDateTime), "MM/dd/yyyy")))
			dtEndDate = DateAdd(DateInterval.Day, -1, CDate(Format$(CurrentDateTime, "MM/dd/yyyy")))
			moInterfaceHistory.MarkStart(2, dtProcessingDateTime)
			TraceWrite("Start processing " & Format$(dtProcessingDateTime, "MM/dd/yyyy HH:mm:ss"), sRoutine)
			If Not BiometricsExport(CurrentDateTime, dtStartDate, dtEndDate) Then Throw New Exception("Error processing biometrics export")
			TraceWrite("End processing " & Format$(dtProcessingDateTime, "MM/dd/yyyy HH:mm:ss"), sRoutine)
			moInterfaceHistory.MarkEnd(1, dtProcessingDateTime)
			oProcessed.Insert(iProcessingTypeID, sCurrentDate, ProcessingTime)
			TraceWrite("End processing for Aetna EMR Interface" & ", " & sCurrentDate & " " & ProcessingTime, sRoutine)
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
		Finally
			oProcessed = Nothing
		End Try
	End Sub
#End Region

#Region "WriteRoutines"
	Private Function BiometricsExport(CurrentDateTime As Date, StartDate As Date, EndDate As Date) As Boolean
		Dim sRoutine As String = "BiometricsExport"
		Dim sNotValued As String = vbNullString
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim sOutRecord, sFileName, sPathFileName, sTestName, sLOINC, sResText, sResNo As String
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iRecordsRead, iRecordsWritten As Int32
		Dim dHDID As Double

		BiometricsExport = False
		sFileName = Replace(gsAetnaBiometricsFileName, "xxxxx", gsAetnaClientID)
		sFileName = Replace(sFileName, "MMDDYYYY", Format$(CurrentDateTime, "MMddyyyy"))
		sPathFileName = gsAetnaDestination & sFileName

		Try
			If File.Exists(sPathFileName) Then
				File.Delete(sPathFileName)
				If File.Exists(sPathFileName) Then Throw New Exception("Error: File " & sPathFileName & " already exists.")
			End If
			oFileStream = New FileStream(sPathFileName, FileMode.Append, FileAccess.Write)
			oStreamWriter = New StreamWriter(oFileStream)
			iRecordsRead = 0
			iRecordsWritten = 0
			If Not OpenDB(oDBC, 2) Then
				CloseOutput(oStreamWriter, oFileStream)
				DeleteFile(sPathFileName)
				Throw New Exception("The database couldn't be accessed.")
			End If
			oCmd.CommandText = "sAetnaBiometricsVitals"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giSQLTimeOutExtended
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = StartDate
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = EndDate
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			sOutRecord = "AETNACARDHOLDERID|HICIN|SRTDATE|ENDDATE|PROVIDERNPI|PROVIDERTAXID|PROVIDERLASTNAME|PROVIDERFIRSTNAME|PROVIDERADDRESSLINE1|PROVIDERADDRESSLINE2|" &
				"PROVIDERCITY|PROVIDERSTATE|PROVIDERZIP|PTZIP|PTLNAME|PTFNAME|PTDOB|PTSEX|LOINC|TESTNAME|RESNO|RESTEXT"
			oStreamWriter.WriteLine(sOutRecord)
			If oRdr.HasRows Then
				While oRdr.Read()
					iRecordsRead += 1
					sOutRecord = vbNullString
					dHDID = SetDouble(oRdr!HDID)
					sOutRecord = vbNullString
					AddField(sOutRecord, SetString(oRdr!InsuranceInfoIDNo), 0) 'AETNACARDHOLDERID
					AddField(sOutRecord, vbNullString, 60) 'HICIN not used
					AddField(sOutRecord, FormatData(SetString(oRdr!OBSDATE), 2), 8)
					AddField(sOutRecord, FormatData(SetString(oRdr!OBSDATE), 2), 8)
					If LCase$(SetString(oRdr!ProviderLastName)) <> "linklogic" AndAlso (LCase$(SetString(oRdr!ProviderLastName)) <> "transcription" And LCase$(SetString(oRdr!ProviderFirstName)) <> "administrator") Then
						AddField(sOutRecord, SetString(oRdr!NPI), 0) 'PROVIDERNPI
						AddField(sOutRecord, vbNullString, 0) 'PROVIDERTAXID
						AddField(sOutRecord, SetString(oRdr!ProviderLastName), 0) 'PROVIDERLASTNAME
						AddField(sOutRecord, SetString(oRdr!ProviderFirstName), 0) 'PROVIDERFIRSTNAME
						AddField(sOutRecord, SetString(oRdr!ProviderAddress1), 0) 'PROVIDERADDRESSLINE1
						AddField(sOutRecord, SetString(oRdr!ProviderAddress2), 0) 'PROVIDERADDRESSLINE2
						AddField(sOutRecord, SetString(oRdr!ProviderCity), 0) 'PROVIDERCITY
						AddField(sOutRecord, SetString(oRdr!ProviderState), 0) 'PROVIDERSTATE
						AddField(sOutRecord, SetString(oRdr!ProviderZipcode), 0) 'PTZIP
					Else
						AddField(sOutRecord, sNotValued, 0)
						AddField(sOutRecord, sNotValued, 0)
						AddField(sOutRecord, sNotValued, 0)
						AddField(sOutRecord, sNotValued, 0)
						AddField(sOutRecord, sNotValued, 0)
						AddField(sOutRecord, sNotValued, 0)
						AddField(sOutRecord, sNotValued, 0)
						AddField(sOutRecord, sNotValued, 0)
						AddField(sOutRecord, sNotValued, 0)
					End If
					AddField(sOutRecord, SetString(oRdr!PatientZipcode), 0) 'PTZIP
					AddField(sOutRecord, SetString(oRdr!PatientLastName), 0) 'PTLNAME
					AddField(sOutRecord, SetString(oRdr!PatientFirstName), 0) 'PTFNAME
					AddField(sOutRecord, FormatData(SetString(oRdr!DateOfBirth), 2), 8) 'PTDOB
					AddField(sOutRecord, FormatData(SetString(oRdr!Gender), 3), 1) 'PTSEX
					Select Case dHDID
						Case 8
							sTestName = "Glucose"
							sLOINC = "1558-6"
						Case 14
							sTestName = "Cholesterol"
							sLOINC = "2093-3"
						Case 28
							sTestName = "A1C"
							sLOINC = "4548-4"
						Case 53
							sTestName = "DiastolicBP"
							sLOINC = "8462-4"
						Case 54
							sTestName = "SystolicBP"
							sLOINC = "8480-6"
						Case 55
							sTestName = "Height"
							sLOINC = "3138-5"
						Case 61
							sTestName = "Weight"
							sLOINC = "3142-7"
						Case 2788
							sTestName = "BMI"
							sLOINC = "41909-3"
						Case Else
							sTestName = vbNullString
							sLOINC = vbNullString
					End Select
					AddField(sOutRecord, sLOINC, 0) 'LOINC
					AddField(sOutRecord, sTestName, 0) 'TESTNAME
					sResNo = SetString(oRdr!OBSVALUE)
					AddField(sOutRecord, sResNo.Replace("%", vbNullString), 0) 'RESNO
					Select Case dHDID
						Case 8
							sResText = vbNullString
						Case 14
							sResText = vbNullString
						Case 28
							sResText = vbNullString
						Case 53
							sResText = vbNullString
						Case 54
							sResText = vbNullString
						Case 55
							sResText = "Inches"
						Case 61
							sResText = "Pounds"
						Case 2788
							sResText = vbNullString
						Case Else
							sResText = vbNullString
					End Select
					AddField(sOutRecord, sResText, 0) 'RESTEXT
					oStreamWriter.WriteLine(sOutRecord)
					iRecordsWritten += 1
				End While
				CloseOutput(oStreamWriter, oFileStream)
				If Not moInterfaceHistory.MarkFileCreate(sPathFileName, iRecordsRead, iRecordsWritten) Then Throw New Exception("Error writing the tracking information." & vbCrLf & "Processing Canceled!")
				BiometricsExport = True
			Else
				Throw New Exception("No patient records were found.")
			End If
		Catch oException As Exception
			TraceWrite("Error:  " & oException.Message, sRoutine)
			CloseOutput(oStreamWriter, oFileStream)
			DeleteFile(sPathFileName)
			BiometricsExport = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region
End Module
