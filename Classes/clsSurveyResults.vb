﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class SurveyResults
#Region "Declartions"
  Private miQuestionID As Int32
  Private msErrorMessage As String
#End Region

#Region "Class Properties"
  Public ReadOnly Property ErrorMessage As String
    Get
      Return msErrorMessage
    End Get
  End Property
#End Region

#Region "Routines"
  Public Sub New()
    Initialize()
  End Sub

  Private Sub Initialize()
    miQuestionID = 0
    msErrorMessage = vbNullString
  End Sub

  Private Function GetQuestionID(Service As String, Variable As String) As Int32
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oCmd As OleDbCommand = New OleDbCommand
    Dim oRdr As OleDbDataReader = Nothing

    If Not OpenDB(oDBC) Then Throw New Exception("SurveyResults.UpdateQuestion: Error opening the database.")
    Try
      oCmd.CommandText = "sQuestionByServiceVariable"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@Service", OleDbType.VarChar, 25).Value = Service
      oCmd.Parameters.Add("@Variable", OleDbType.VarChar, 25).Value = Variable
      oCmd.Connection = oDBC
      oRdr = oCmd.ExecuteReader(CommandBehavior.SingleRow)
      If oRdr.HasRows Then
        oRdr.Read()
        GetQuestionID = SetInt32(oRdr!QuestionID)
      Else
        GetQuestionID = 0
      End If
    Catch oException As Exception
      GetQuestionID = 0
    Finally
      CloseRDR(oRdr)
      CloseCMD(oCmd)
      CloseDB(oDBC)
    End Try
  End Function
#End Region

#Region "UpdateQuestion"
  Public Function UpdateQuestion(FileProcessingID As Int32, Service As String, Variable As String, Question As String) As Boolean
    Dim oDBC As OleDbConnection = New OleDbConnection
    Dim oSCmd As OleDbCommand = New OleDbCommand
    Dim oICmd As OleDbCommand = New OleDbCommand
    Dim oUCmd As OleDbCommand = New OleDbCommand
    Dim oDataAdapter As New OleDbDataAdapter
    Dim oDataTable As New DataTable
    Dim oDataRow As DataRow
    Dim bIsNew As Boolean
    Dim iQuestionID As Int32 = GetQuestionID(Service, Variable)

    If Not OpenDB(oDBC) Then Throw New Exception("SurveyResults.UpdateQuestion: Error opening the database.")

    Try
      oSCmd.CommandText = "sQuestion"
      oSCmd.CommandType = CommandType.StoredProcedure
      oSCmd.Parameters.Add("@QuestionID", OleDbType.BigInt).Value = iQuestionID
      oSCmd.Connection = oDBC

      oICmd.CommandText = "iQuestion"
      oICmd.CommandType = CommandType.StoredProcedure
      oICmd.Parameters.Add("@FileProcessingID", OleDbType.BigInt).SourceColumn = "FileProcessingID"
      oICmd.Parameters.Add("@Service", OleDbType.VarChar, 25).SourceColumn = "Service"
      oICmd.Parameters.Add("@Variable", OleDbType.VarChar, 25).SourceColumn = "Variable"
      oICmd.Parameters.Add("@Question", OleDbType.VarChar, 512).SourceColumn = "Question"
      oICmd.Parameters.Add("@NewID", OleDbType.BigInt).Direction = ParameterDirection.Output
      oICmd.Connection = oDBC

      oUCmd.CommandText = "uQuestion"
      oUCmd.CommandType = CommandType.StoredProcedure
      oUCmd.Parameters.Add("@QuestionID", OleDbType.BigInt).Value = iQuestionID
      oUCmd.Parameters("@QuestionID").SourceColumn = "QuestionID"
      oUCmd.Parameters("@QuestionID").SourceVersion = DataRowVersion.Original
      oUCmd.Parameters.Add("@FileProcessingID", OleDbType.BigInt).SourceColumn = "FileProcessingID"
      oUCmd.Parameters.Add("@Service", OleDbType.VarChar, 25).SourceColumn = "Service"
      oUCmd.Parameters.Add("@Variable", OleDbType.VarChar, 25).SourceColumn = "Variable"
      oUCmd.Parameters.Add("@Question", OleDbType.VarChar, 512).SourceColumn = "Question"
      oUCmd.Connection = oDBC

      oDataAdapter.SelectCommand = oSCmd
      oDataAdapter.InsertCommand = oICmd
      oDataAdapter.UpdateCommand = oUCmd

      oDataAdapter.Fill(oDataTable)
      If oDataTable.Rows.Count = 0 Then
        oDataRow = oDataTable.NewRow
        oDataTable.Rows.Add(oDataRow)
        bIsNew = True
      End If

      oDataTable.Rows(0).Item("FileProcessingID") = FileProcessingID
      oDataTable.Rows(0).Item("Service") = Service
      oDataTable.Rows(0).Item("Variable") = Variable
      oDataTable.Rows(0).Item("Question") = Question
      oDataAdapter.Update(oDataTable)
      If bIsNew Then miQuestionID = SetInt32(oICmd.Parameters("@NewID").Value)
      UpdateQuestion = True
    Catch oException As Exception
      msErrorMessage = oException.Message
      UpdateQuestion = False
    Finally
      CloseCMD(oSCmd)
      CloseCMD(oICmd)
      CloseCMD(oUCmd)
      CloseDB(oDBC)
      oDataAdapter.Dispose()
      oDataTable.Dispose()
      oDataRow = Nothing
    End Try
  End Function
#End Region
End Class

