#Region "Init"
Option Explicit On
Option Strict On
Imports System.IO
Imports System.Data.OleDb
#End Region

Public Class InitializeApplication
#Region "Class Properties"
	Public Property LogPath As String
	Public Property Trace As Boolean
	Public Property ServicePauseTime As Int32
	Public Property EMailRecipient As ArrayList
	Public Property EMailSender As String
	Public Property SendEMail As Boolean
	Public Property StatusEMail As Boolean
	Public Property StatusEMailTime As ArrayList
	Public Property ProcessingTime As ArrayList
	Public Property AetnaDestination As String
	Public Property AetnaClientID As String
	Public Property AetnaClearFileName As String
	Public Property AetnaBiometricsFileName As String
	Public Property AetnaProcessingDays As Int32
#End Region

#Region "Initialize"
	Private Sub Initialize()
		Exit Sub
	End Sub
#End Region

#Region "New"
	Public Sub New()
		Initialize()
	End Sub
#End Region

#Region "GetSettings"
	Public Function GetSettings(INIPathFileName As String) As Boolean
		Dim oFileStream As New FileStream(INIPathFileName, FileMode.Open, FileAccess.Read)
		Dim oStreamReader As New StreamReader(oFileStream)
		Dim sInRecord As String, sItemType As String, sItemSetting As String
		Dim iINICounter As Int32 = 0
		Const iINICount As Int32 = 14
		Dim iWork As Integer
		Dim bProcessingTimeFound As Boolean = False
		Dim bStatusEMailTimeFound As Boolean = False
		Dim bEMailRecipientFound As Boolean = False

		Try
			GetSettings = False
			EMailRecipient = New ArrayList
			StatusEMailTime = New ArrayList
			ProcessingTime = New ArrayList
			StatusEMailTime = New ArrayList
			oStreamReader.BaseStream.Seek(0, SeekOrigin.Begin)
			While oStreamReader.Peek() > -1
				sInRecord = Trim$(oStreamReader.ReadLine())
				If Len(sInRecord) > 0 Then
					If Mid$(sInRecord, 1, 1) <> ";" Then
						iWork = InStr(1, sInRecord, ";")
						If iWork > 0 Then sInRecord = Trim$(Mid$(sInRecord, 1, iWork - 1))
						iWork = InStr(1, sInRecord, " ")
						If iWork > 3 Then
							If iWork < Len(sInRecord) Then
								sItemType = Trim$(LCase$(Mid$(sInRecord, 1, iWork - 1)))
								sItemSetting = Trim$(Mid$(sInRecord, iWork + 1, Len(sInRecord)))
								Select Case sItemType
									Case "[logpath]"
										LogPath = sItemSetting
										iINICounter += 1
									Case "[trace]"
										If LCase$(SetString(sItemSetting)) = "true" Then
											Trace = True
										Else
											Trace = False
										End If
										iINICounter += 1
									Case "[servicepausetime]"
										ServicePauseTime = SetInt32(sItemSetting) * 60000
										iINICounter += 1
									Case "[emailrecipient]"
										EMailRecipient.Add(Replace(SetString(sItemSetting), ",", ";"))
										If Not bEMailRecipientFound Then
											iINICounter += 1
											bEMailRecipientFound = True
										End If
									Case "[emailsender]"
										EMailSender = SetString(sItemSetting)
										iINICounter += 1
									Case "[sendemail]"
										If LCase$(SetString(sItemSetting)) = "true" Then
											SendEMail = True
										Else
											SendEMail = False
										End If
										iINICounter += 1
									Case "[statusemail]"
										StatusEMail = LCase$(SetString(sItemSetting)) = "true"
										iINICounter += 1
									Case "[statusemailtime]"
										StatusEMailTime.Add(SetString(sItemSetting))
										If Not bStatusEMailTimeFound Then
											iINICounter += 1
											bStatusEMailTimeFound = True
										End If
									Case "[processingtime]"
										ProcessingTime.Add(sItemSetting)
										If Not bProcessingTimeFound Then
											iINICounter += 1
											bProcessingTimeFound = True
										End If
									Case "[aetnadestination]"
										AetnaDestination = SetString(sItemSetting)
										iINICounter += 1
									Case "[aetnaclientid]"
										AetnaClientID = SetString(sItemSetting)
										iINICounter += 1
									Case "[aetnaclearfilename]"
										AetnaClearFileName = SetString(sItemSetting)
										iINICounter += 1
									Case "[aetnabiometricsfilename]"
										AetnaBiometricsFileName = SetString(sItemSetting)
										iINICounter += 1
									Case "[aetnaprocessingdays]"
										AetnaProcessingDays = SetInt32(sItemSetting)
										iINICounter += 1
								End Select
							End If
						End If
					End If
				End If
			End While
			oStreamReader.Close()
			oFileStream.Close()
			If iINICount <> iINICounter Then
				WriteEventLog("InitializeApplication", "Error: Incorrect ini count, entries are missing.", EventLogEntryType.Error)
			Else
				GetSettings = True
			End If
		Catch oException As Exception
			WriteEventLog("InitializeApplication", "Error: " & oException.Message, EventLogEntryType.Error)
			GetSettings = False
		Finally
		End Try
	End Function
#End Region

#Region "Version Check"
	Public ReadOnly Property VersionCheck(ByVal Version As String) As Boolean
		Get
			Dim oDBC As OleDbConnection = New OleDbConnection
			Dim oCmd As OleDbCommand = New OleDbCommand
			Dim oRdr As OleDbDataReader = Nothing
			Dim bWork As Boolean

			If Not OpenDB(oDBC) Then Return False
			oCmd.CommandText = "sVersionCheck"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@Version", OleDbType.VarChar, 25).Value = Version
			Try
				oCmd.Connection = oDBC
				oRdr = oCmd.ExecuteReader
				If oRdr.HasRows Then
					bWork = True
				Else
					bWork = False
				End If
			Catch oException As Exception
				bWork = False
			Finally
				CloseRDR(oRdr)
				CloseCMD(oCmd)
				CloseDB(oDBC)
			End Try
			Return bWork
		End Get
	End Property
#End Region
End Class